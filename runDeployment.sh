o "Deploying app.jar to docker folder"
packageName=`ls target/common-config-server-1*.jar`
versionid=`echo $packageName | awk -F "-" '{ print $2}'`
versionname=`echo $packageName | awk -F "-" '{ print $3}' | awk -F "." '{ print $1}'`
version=`echo $versionid-$versionname`
echo "version: $version"
cp -r $packageName deployment/app.jar
dockerImageName=pier12/config-server
dockerRedName=employee
dockerContainerName=config-server
dockerpid=`docker ps -a | grep $dockerImageName | grep "Up" | awk -F " " '{ print $1 }'`
if [[ $dockerpid != "" ]];then 
   docker kill $dockerpid
   docker rm $dockerpid
fi
docker build -t $dockerImageName deployment/.
#docker run -d --name=$dockerContainerName --mount type=bind,source=C:\static,target=/opt/static --network=$dockerRedName -p 8008:8008 $dockerImageName 
dockerImageId=`docker images | grep $dockerImageName | grep latest | awk -F " " '{print $3}'` 
docker tag $dockerImageId $dockerImageName:$version
docker login -u dwilsonc -p b1n9e8t9
docker push $dockerImageName:$version
