node{
	
	def mvnHome
	
	stage('Preparation') {
		try{
			git url:'https://gitlab.com/pier12/cloud-config-server.git', branch:'master'
			mvnHome= tool 'M2'
		}catch(e){
			notifyStarted("Error al clonar proyecto config-server de Gitlab")
			throw e
		}
	}
	
	stage('Test') {
		try{
			sh "'${mvnHome}/bin/mvn' test"
		}catch(e){
			notifyStarted("Error al realizar Test al proyecto config-server")
			throw e
		}
	}
	
	stage('Build') {
		try{
			sh "'${mvnHome}/bin/mvn' clean package -DskipTestes"
		}catch(e){
			notifyStarted("Error al construir el proyecto config-server")
			throw e
		}
	}
	
	stage('Packaging') {
		try{
			archive 'target/*.jar'
		}catch(e){
			notifyStarted("Error al empaquetar .JAR del proyecto config-server")
			throw e
		}
	}
	
	stage('Deployment') {
		try{
			sh '/var/lib/jenkins/workspace/EscuelaJava/PierReyes/Config-Server-DEV/runDeployment.sh'
		}catch(e){
			notifyStarted("Error al desplegar el proyecto config-server")
			throw e
		}
	}
	
	stage('Notification') {
		notifyStarted("Tu codigo ha sido testeado y desplegado con exito!!")
	}
}

def notifyStarted(String message) {
	slackSend (color: '#FFFF00', message: "${message}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
}